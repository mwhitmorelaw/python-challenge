# PyPoll and PyBank
## PyPoll
This script is intended to help a small, rural town modernize its vote-counting process. (Up until now, Uncle Cleetus had been trustfully tallying them one-by-one, but unfortunately, his concentration isn't what it used to be.)
## PyBand
This script is intended to analyze the financial records of a company, given a set of financial data called budget_data.csv. The dataset is composed of two columns: Date and Profit/Losses. (Thankfully, the company has rather lax standards for accounting so the records are simple.)
## Getting Started

Navigate into the 'python-challenge' directory from here you can navigate to either the 'PyPoll' directory or the 'PyBank' directory. Each of these sub-directories contains a file called 'main.py', that can be ran by typing 'python PyBank/main.py' or 'python PyPoll/main.py' in your terminal from the 'python-challenge' directory and hitting enter. 

### Prerequisites

This project requires python 3 to be installed (and assumes python 3 is on your terminal's path). This can be verified by opening your terminal and type 'python --version' (without the quotes). If you have Python 3 installed and on your path you should see 'Python 3.x.x' (the x's are any number) printed to your terminal. If not, head over to wwww.python.org/downloads and follow the install instructions.

### Installing

This project uses only standard library packages, thus a development environment is not necessary.

## Running the tests

The automated tests are contained in the 'tests' folder. The tests are written using the build-in 'unittest' framework. 

In order to run these tests, navigate to the 'python-challenge' folder and enter 'python -m unittest' (without the quotes).

### Break down into end to end tests

The scripts included in this project are self-contained. The included unit tests act as end-to-end tests.

## Deployment

The scripts included in this project are self-contained and intended to be used for one-off runs. Assuming you feel these scripts should be deployed to production, no additional steps are needed beyond those stated in 'Getting Started' above.

## Built With

* [Python](http://www.python.org) - The web framework used

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. The version is currently 1.0.0.

## Authors

* **Michael Whitmore** - *All work* - [mwhitmorelaw](https://gitlab.com/mwhitmorelaw)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Guido van Rossum
