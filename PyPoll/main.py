import csv
import os


class PyPollResults(object):
    def __init__(self, file_name):
        self.candidates = {}
        self.__analyze_file(file_name)

    def format(self):
        return '\n'.join([
            "Election Results",
            "-------------------------",
            f"Total Votes: {self.total_votes}",
            "-------------------------",
        ] + [f"{name}: {self.formatted_percent_of_votes(count)}% ({count})"
             for name, count in self.candidates.items()
             ] + [
            "-------------------------",
            f"Winner: {self.winner[0]}",
            "-------------------------",
        ])

    # this should only be called once in the lifetime of this object, so
    # that the instance never changes.
    def __analyze_file(self, file_name):
        with open(file_name, 'r') as file:
            reader = csv.reader(file)

            # read the first row to set the columns
            columns = reader.__next__()

            for row in reader:
                candidate = f'{row[1]} {row[2]}'
                if candidate in self.candidates:
                    self.candidates[candidate] += 1
                else:
                    self.candidates[candidate] = 1

    @property
    def total_votes(self):
        return sum(self.candidates.values())

    @property
    def winner(self):
        most_votes = max(self.candidates.values())
        return [c for c in self.candidates.items() if c[1] == most_votes][0]

    def formatted_percent_of_votes(self, count):
        percent = count/self.total_votes * 100
        return format(percent, '.3f')


if __name__ == '__main__':
    base = os.path.dirname(__file__)
    file_name = base + '/Resources/election_data.csv'
    results = PyPollResults(file_name).format()
    print(results)
    with open(base + '/results.txt', 'w') as f:
        f.write(results)
