import csv
from decimal import Decimal
from statistics import mean
import os


class PyBankResults(object):
    def __init__(self, file_name):
        self.months = set()
        self.total_profit = 0
        self.profit_changes = []
        self.best_month = (None, 0)
        self.worst_month = (None, 0)
        self.__analyze_file(file_name)

    def format(self):
        return '\n'.join([
            f"Total Months: {len(self.months)}",
            f"Total: ${self.total_profit}",
            f"Average Change: ${ self.formatted_avg_profit_change }",
            f"Greatest Increase in Profits: {self.best_month[0]} (${self.best_month[1]})",
            f"Greatest Decrease in Profits: {self.worst_month[0]} (${self.worst_month[1]})",
        ])

    @property
    def formatted_avg_profit_change(self):
        return format(mean(self.profit_changes), '.2f')

    # this should only be called once in the lifetime of this object, so
    # that the instance never changes.
    def __analyze_file(self, file_name):
        with open(file_name, 'r') as f:
            reader = csv.reader(f)

            # read the first row to set the columns
            columns = reader.__next__()

            last_profit = None
            for row in reader:
                month = row[0]
                self.months.add(month)

                profit = Decimal(row[1])
                self.total_profit += profit
                self.__handle_profit_change(profit, last_profit, month)

                # cache profit for the current month so that it can be compared
                # to the profit of the following month on the next iteration.
                last_profit = profit

    def __handle_profit_change(self, profit, last_profit, month):

        # there can be no change on the first month; move on.
        is_first_month = len(self.months) == 1
        if is_first_month:
            return

        profit_change = profit - last_profit

        self.profit_changes.append(profit_change)

        # check the profit change for the best/worst month beats the
        # current best/worst. if there is no best/worst month then
        # the current month will be assigned to both.

        # could be refactored to keep logic stateless, but easy enough to
        # as is.
        if (profit_change > self.best_month[1] or
                self.best_month[0] is None):
            self.best_month = (month, profit_change)

        if (profit_change < self.worst_month[1] or
                self.worst_month[0] is None):
            self.worst_month = (month, profit_change)


def main():
    base = os.path.dirname(__file__)
    file = base + '/Resources/budget_data.csv'
    results = PyBankResults(file)

    formatted_results = results.format()
    print(formatted_results)
    with open(base + '/results.txt', 'w') as f:
        f.write(formatted_results)


if __name__ == '__main__':
    main()
