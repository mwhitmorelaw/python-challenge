import os
import unittest

from PyPoll.main import PyPollResults


class TestPyPollResults(unittest.TestCase):
    def test_analyze_file(self):
        test_file = os.path.dirname(__file__) + '/Resources/election_data.test.csv'
        test_results = PyPollResults(test_file)

        self.assertSequenceEqual({
            'Marsh Khan': 5,
            'Marsh Correy': 2,
            'Queen Khan': 1,
            'Marsh Li': 1,
            'Queen Correy': 1,
        }, test_results.candidates)
        self.assertSequenceEqual(test_results.winner, ('Marsh Khan', 5))
        self.assertEqual(test_results.total_votes, 10)


if __name__ == '__main__':
    unittest.main()
