import os
import unittest

from PyBank.main import PyBankResults


class TestPyBankResults(unittest.TestCase):
    def test_analyze_file(self):
        test_file = os.path.dirname(__file__) + '/Resources/budget_data.test.csv'
        test_results = PyBankResults(test_file)

        self.assertEqual(len(test_results.months), 5)
        self.assertEqual(test_results.total_profit, 2415638)
        changes = [
            984655 - 867884,
            322013 - 984655,
            -69417 - 322013,
            310503 - -69417
        ]
        avg_profit_change = sum(changes)/4

        self.assertEqual(float(sum(test_results.profit_changes)/4), avg_profit_change)
        self.assertSequenceEqual(test_results.best_month, ('May-2010', max(changes)))
        self.assertSequenceEqual(test_results.worst_month, ('Mar-2010', min(changes)))


if __name__ == '__main__':
    unittest.main()
